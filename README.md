/*  
 * <著作権及び免責事項>  
 *  
 * 　本ソフトウェアはフリーソフトです．自由にご使用下さい．  
 *  
 * このソフトウェアを使用したことによって生じたすべての障害,損害,不具合等に関しては,  
 * 私と私の関係者及び,私の所属する団体とも,一切の責任を負いません．  
 * 各自の責任においてご使用下さい  
 *   
 *   
 *  
 * この<著作権及び免責事項>があるソースに関しましては,すべて以下の者が作成しました．  
 * 作成者 : tkmoteki  
 * 連絡先 : t-moteki@hykwlab.org  
 *  
 */  


# 組込みOS armプロジェクト
* * * * *

# arm-osについて

* * * * *

arm-osは、armプラットフォームで動くオペレーティングシステムである  
本osは、シュミレーションではなく完全実機で動作させる事が可能であり、OSのkernelをリアルにコントロールする事が可能   
また、ユーザが自由にアプリケーションを作成し動作させることも可能
基本的にクロス実行させるので、出力やコントロールにLinuxが必要になる  

このドキュメントではarm-osの起動、およびアプリケーション(タスク)の起動について述べる  
アカデミックなドキュメントについてはプロジェクトdocディレクトリを参照して下さい  


# OSの起動

* * * * *

**必要になるもの**

* ターゲットハードウェア(実機)
	* 電源ケーブル
	* シリアルtoUSBケーブル([例](http://www.henj.in/USB.html))
		* (ドライバ)	
* Linux(仮想マシン、またはネイティブ)
	* シリアルコンソール

基本的な流れとして、  
ターゲットハードウェアを電源につなぎ、  
シリアルでPCとターゲットハードウェアを接続する  
接続後はPC上のlinuxで出力やコントロールをする  

## ターゲットハードウェア

* * * * *

ターゲットはbeagle boradXM

* [beagle borad XM](http://beagleboard.org/Products/BeagleBoard-xM)


### ドライバ関連

* * * * *

linuxをネイティブで使用しない場合(ホストOSがwindows7、macで仮想マシンを使用など)、シリアルtoUSBケーブルのドライバが必要になる事がある  
必要か、必要でないかは[こちらを参照](http://www.henj.in/USB.html)

シリアルtoUSBケーブルに使用されているチップを調べ(PL2303)、
チップメーカーさんのHP調べる  
大概([こちら](http://www.prolific.com.tw/US/ShowProduct.aspx?pcid=41&showlevel=0041-0041))のドライバを使用している  

#### ホストOSがmacの場合  

* * * * *

ダウンロードしたドライバをインストールする前に、
古い同社のドライバが存在する時は、消しておく(ドライバ付属のreadme参照)  

````
hykw-MacBook-Air:~ test$ ls /System/Library/Extensions/ | grep 'Prolific'
ProlificUsbSerial.kext
hykw-MacBook-Air:~ test$
hykw-MacBook-Air:~ test$ sudo rm -rf /System/Library/Extensions/ProlificUsbSerial.kext
````

ドライバを入れたら再起動  

実機を接続し、  
automountしているか、ターミナルで確認してみる(Macは/Volumesではなく/dev)  

デバイスファイル名がわからないときは、以下のようなコマンドをうちつつ、シリアルtoUSBケーブルを抜いたり、指したりを繰り返せばいい  

````
hykw-MacBook-Air:~ test$ ls -alt /dev | head
total 9
crw--w----   1 test  tty          16,   0  8 23 18:23 ttys000
crw-rw-rw-   1 root  tty          15,   1  8 23 18:23 ptmx
crw-rw-rw-   1 root  wheel        18,  15  8 23 18:23 cu.usbserial
crw-rw-rw-   1 root  wheel        18,  14  8 23 18:23 tty.usbserial
crw-rw-rw-   1 root  wheel         3,   2  8 23 18:23 null
crw-------   1 test  staff         0,   0  8 23 18:20 console
crw-rw-rw-   1 root  wheel        13,   0  8 23 18:18 random
crw-rw----   1 root  access_bpf   23,   1  8 23 17:38 bpf1
crw-rw----   1 root  access_bpf   23,   2  8 23 17:38 bpf2
hykw-MacBook-Air:~ test$
````

ここでは、tty.usbserialがデバイスのスペシャルファイル

#### ホストOSがwindowsの場合

* * * * *

ドライバをダウンロード、およびインストール後  
再起動をし実機を接続する  
コントロールパネルからデバイスマネージャーを開き、COMポートを確認する  

読み込まれない場合は、プロパティからドライバの更新を行い  
インストールしたドライバを選択する  


## Linux

* * * * *

Linuxを仮想マシンで動作させる場合は、実機を接続しマウントしておく  

以下の仮想マシンで動作確認済み

* VirtaulBox
* VMware fusion
* Parallels Desktop


## シリアルコンソール

* * * * *

arm-osの出力やコントロールをするためにシリアルコンソールを用意する(プロジェクトの中のscriptを使ってインストール自動化も可能)  
以下で動作確認済み  

* cu
* minicom
* kermit

cuでの接続例を紹介する  
デバイスファイルを確認する

~~~~
(✿☉｡☉)ﾊｯ☀  bin [master] ls -alt /dev/ | head       
合計 4
crw-rw-rw-   1 root    tty       5,   2 11月  3 19:46 ptmx
crw-rw----   1 root    dialout 188,   0 11月  3 19:43 ttyUSB0
crw-rw-rw-   1 root    tty       5,   0 11月  3 19:37 tty
crw-------   1 root    root      5,   1 11月  3 19:36 console
drwxr-xr-x  16 root    root        4120 11月  3 19:34 .
drwxr-xr-x   2 root    root        3520 11月  3 19:34 char
drwxr-xr-x   4 root    root          80 11月  3 19:34 serial
crw-rw----   1 root    video    29,   0 11月  3 19:34 fb0
drwxr-xr-x   3 root    root         180 11月  3 19:34 snd
~~~~

上記の例だと、デバイスファイルは/dev/ttyUSB0  

cuでデバイスファイルとボーレートを115200として接続する  
なお、パーミッションなどは適時変更する  

~~~~
(✿☉｡☉)ﾊｯ☀  bin [master] cu -l /dev/ttyUSB0 -s 115200
Connected. 
~~~~

接続確認できたら
ブートローダとarm-osの起動を行う  
これらについては、下記のarm-osとubootの操作を参照  


### VirtualboxにおけるUSB機器の取り扱いについて
* * * * *

 **virtualboxはUSB機器がマウントできない事がある**

マウントできない時は、以下の事を試す  

````

事前準備・・・対象のUSB機器をMacのUSBポートに接続する

１、仮想マシンの電源を切る（シャットダウン）する。

２、仮想マシンの設定を開く→ポート→USB

３、「USBコントローラを有効化」「USB 2.0 (EHCI) コントローラを有効化」にチェックを入れる。

４、USBコネクタに＋マークがついているボタン（＋は緑色、上から２番目のちっちゃいボタン）をクリック、対象のUSB機器を選ぶ

５、「USBデバイスフィルタ」のリストに対象の機器が現れるので、それをダブルクリック

６、メーカーと製品名とシリアル番号の欄を空欄にする（一番上の入力欄[名前]はいじらない）→OK

７、設定画面もOKをクリックして閉じる

８、対象のUSB機器のUSBケーブルをMacから物理的に取り外す

９、仮想マシン起動

１０、対象のUSB機器のUSBケーブルをMacから物理的にとりつける

１１、数秒のラグがあるが、USBアイコンの右下に黄色や緑のアイコンがチカチカしだして認識される

　　　仮想マシンウインドウの右下にあるUSBアイコンにカーソルをあわせると「アクティブなUSBデバイスを表示：対象のUSBデバイス」みたいな形になる

　　　ダメなとき、アイコンを右クリック

　　　　→対象のデバイスは表示されているが、対象のデバイス名がグレーアウトしている

　　　　　　上記４～８ぐらいが失敗している

　　　　→対象のデバイスが表示されない

　　　　　　仮想マシンはさわらずに一度USBケーブルを物理的に抜く

　　　　　　対象の機器に電源スイッチがあれば一度きって入れる、もう一度差し込む

　　　　　　このあとゲストOSヘ認識されるか確認

````

## arm-os

* * * * *

まずセットアップとして、

1. beagleboradxMにデフォルトで入っているSDカードのosファイル(UIMAGE)を削除する
2. Narcissus-rootfsの中身のファイルをすべて削除する(Narcissus-rootfs/bootの空ディレクトリだけ残しておく(bootの中身はすべて削除))

**パーティションのbootとNarcissus-rootfsはこのままにしておく

## ブートローダ(uboot)の操作

* * * * *

ブートローダはubootを採用している  

ubootの操作とは、カーネルの起動    
主に2つのパターンがある  

* ファイルシステムから起動
* 実行ファイルをターゲットへ転送して起動

#### ファイルシステムから起動

* * * * *

step1)osの 実行ファイルの形式とファイル名、エントリーポイントを確認しておく

デフォルトの設定は、以下の通り  

* 実行ファイル形式は.bin
* 実行ファイル名はuboot.bin
* エントリーポイントは80200000

step2) ファイルシステムに実行ファイルを置いておく

arm-osのところで設定した内容  

step3) ubootの環境変数を確認

* printenv

設定されている環境変数一覧を表示するコマンド

arm-osでは、主に各環境変数にstep1で確認したエントリーポイントの設定を行う(ファイル名の設定はいらない)

* setenv

エントリーポイントの設定など環境変数の設定をするコマンド

````
setenv loadaddr 80400000 //エントリーポイントを0x80400000にする
````

step4) ubootのコマンドでメモリロードおよび起動(エントリーポイントのジャンプ)する

実行ファイル形式bin、実行ファイル名uImage.bin、エントリーポイント80200000

* fatload

fatファイルシステムからメモリへロードするコマンド(環境変数loadaddrを80200000にした場合)

````
fatload mmc 0 80200000 uImage.bin
````

* go

エントリーポイントへジャンプするコマンド(メモリへロードしたアドレスを指定する。ここでは、0x80200000)

````
go 80200000
````

#### 実行ファイルをターゲットへ転送して起動

* * * * *

step1) 実行ファイルの形式とファイル名、エントリーポイントを確認しておく

* 実行ファイル形式は.bin
* 実行ファイル名はuboot.bin
* エントリーポイントは80200000

step2) シリアルコンソールを起動させファイル転送する

minicomを例にあげる(ボーレートとハードウェア、ソフトウェアフロー制御の設定済みとして説明する)  

minicomの起動  

````
(✿☉｡☉)ﾊｯ☀  bin [master] minicom -o
````

ファイル転送(実行ファイル転送)を行う  
modemプロトコルで説明する(tftpも可能)

minicom上で[Ctrl]+[A], [S]を押し転送画面を表示する  

ymodem選択し、転送するファイル名を指定する
このとき、ubootは転送されてきたファイルをメモリへロードする  

step3) ubootのコマンドで起動(エントリーポイントのジャンプ)する

例として、エントリーポイント	80200000とする

````
go 80200000
````

### autoboot

* * * * *

coming soon

### タスクセットの起動とosサポートコマンド

* * * * *

osサポートコマンドではhelpコマンドを使用する  
以下はhelpコマンドの出力の一例である  

````
> help
dump     - memory dump.
echo     - out text serial line.
fatinfo  - print information about filesystem.
fatload  - load binary file from a dos filesystem.
fatls    - list files in a directory (default /).
go       - run task sets.
recvlog  - receive log file over serial line(xmodem mode
sendlog  - send log file over serial line(xmodem mode)
tsetload - load task sets form fat file system.
````

タスクセットの起動はtsetloadで行う  
タスク実行はあらかじめ、タスクの実行ファイルをSDカードに置いておく  

注意) タスク実行ファイルは.tasks/.ファイルとしておく

* fatls

ファイルシステム上のファイルを表示する

````
> fatls mmc 0
 000056d8   mlo 
 00045874   u-boot.bin 
 00000086   uenv.txt 
            .trash-1000/
            .tasks/
 00008f8c   kernel.bin 

4 file(s), 2 dir(s)

````

階層ディレクトリを参照する場合

````
> fatls mmc 0 .tasks
            ./
            ../
 00000a28  .task2 
 00000888  .task3 
 00000960  .task4 
 00000968  .task5 
 000009cc  .task6 
 00000a1c  .task7 
 000008d0  .task8 
 000009cc  .task1 

8 file(s), 2 dir(s)

````

fatloadはタスク単体をロードする
基本的には、タスクセットレベルで動作させるので、tsetload(fatloadラップコマンド)を使う  

````
> 
command unknown.
> tsetload tsk_set3
reading .tasks/.task6
9cc bytes read
fatload successed.
reading .tasks/.task7
a1c bytes read
fatload successed.
reading .tasks/.task8
8d0 bytes read
fatload successed.
````

goコマンドでエントリーポイントへジャンプする  
**tsetloadとgoコマンドで指定するタスクセット名を一致させる(不一致の場合のエラー出力しない)**

````
> go tsk_set3
> sample_tsk6 started.
sample_tsk6 create tsk(sample_tsk7).
sample_tsk6 running in (sample_tsk7).
sample_tsk7 started.
sample_tsk7 create tsk(sample_tsk8).
sample_tsk7 running in (sample_tsk8).
sample_task8 started.
sample_tsk7 running out (sample_tsk8).
sample_tsk6 running out (sample_tsk7).
sample_tsk7 wake up OK(by sample_tsk6).
sample_tsk8 rel_wai (by sample_tsk7).
sample_tsk8 DORMANT.
sample_tsk7 DORMANT.
sample_tsk6 DORMANT.

command unknown.
> 
````
